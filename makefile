DEBUG_FLAGS=-D DEBUG_LEVEL=1 -std=c++11 -Wextra -g -c
RELEASE_FLAGS=-D DEBUG_LEVEL=0 -w -std=c++11 -O2 -c
CXX=g++
CONST_H=strsetconst.h 
CONST_CC=strsetconst.cc
MUT_H=strset.h 
MUT_CC=strset.cc
TARGET=strset.o strsetconst.o
debuglevel=0

.PHONY: default
default: $(CONST_H) $(MUT_H) $(CONST_CC) $(MUT_CC)
ifeq ($(debuglevel), 0)
	$(CXX) $(MUT_CC) $(RELEASE_FLAGS) -o strset.o
	$(CXX) $(CONST_CC) $(RELEASE_FLAGS) -o strsetconst.o
else
	$(CXX) $(MUT_CC) $(DEBUG_FLAGS) -o strset.o
	$(CXX) $(CONST_CC) $(DEBUG_FLAGS) -o strsetconst.o
endif

.PHONY: clean
clean:
	rm -f ./strset.o ./strsetconst.o strsetconst.h.gch strset.h.gch
