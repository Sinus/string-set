#include "strset.h"
#include "strsetconst.h"

#include <map>
#include <set>
#include <string>
#include <iostream>

#if DEBUG_LEVEL == 1
static const bool debug = true;
#else
static const bool debug = false;
#endif

using std::map;
using std::set;
using std::string;
using std::endl;
using std::cerr;
using std::pair;
using std::ios_base;


namespace{ 
    //helper functions and variables that should not be seen outside
    unsigned long currentSetNumber = 1;
    bool initialized = false;

    map<unsigned long, set<string> >& stringSets()
    {
        static map<unsigned long, set<string> > *ssetmap = 
            new map<unsigned long, set<string> >;
        return *ssetmap;
    }


    inline void noSet(unsigned long id)
    {
        cerr << "No set with id = " << id << " found." << endl;
    }

    const set<string>& getStrset(unsigned long id)
    {
        static const set<string> emptySet;

        if (stringSets().count(id) == 0)
        {
            if (debug)
            {
                cerr << "strset_comp: ";
                noSet(id);
            }
            return emptySet;
        }
        else
            return stringSets()[id];
    }

    void init()
    {
        //make sure evrything is ready to work
        if (initialized)
            return;

        ios_base::Init();

        if (debug)
            cerr << "initializing" <<endl;

        set<string> ss42;
        ss42.insert("42");
        stringSets().insert({strset42, ss42});
        initialized = true;

        if (debug)
            cerr << "strset42 and stringSets initialized" <<endl;
    }
}

extern "C" unsigned long strset_new()
{
    //create new set and return it's id
    init();
    if (debug)
        cerr << "strset_new()" << endl;

    if (currentSetNumber == strset42)
    {
        if (debug)
            cerr << "strset_new: Omitting id " << strset42 
                << " taken by str42" << endl;
        currentSetNumber++;
    }

    set<string> newSet;
    stringSets().insert({currentSetNumber, newSet});

    if (debug)
        cerr << "strset_new: set number " << currentSetNumber 
            << " created" << endl;

    return currentSetNumber++;
}

extern "C" void strset_delete(unsigned long id)
{
    //delete a set with given ID
    init();
    if (debug)
        cerr << "strset_delete(" << id << ")" << endl;        

    if (id == strset42)
    {
        if(debug)
            cerr << "strset_delete: can't delete strset42" << endl;
        return;
    }

    if (debug)
    {
        if (stringSets().count(id) == 0){ //there is not such set
            cerr << "strset_delete: ";
            noSet(id);
        }
        else
        {
            stringSets().erase(id);
            cerr << "strset_delete: deleted set number " << id << endl;
        }
    }
    else
        stringSets().erase(id);
    return;
}

extern "C" size_t strset_size(unsigned long id)
{
    //return size of set with given ID
    init();
    if (debug)
        cerr << "strset_size(" << id << ")" << endl;

    if (stringSets().count (id) > 0)
    { //there is a set with this ID
        if (debug)
        {
            cerr << "strset_size: the number of elements in set number "
                << id << " is " << stringSets()[id].size() << endl;
        }
        return stringSets()[id].size();
    }
    else
    { //no set with such ID
        if (debug)
        {
            cerr << "strset_size: ";
            noSet(id);
        }
        return 0;
    }
}

extern "C" void strset_insert(unsigned long id, const char* value)
{
    //insert value to set with given ID
    init();
    if (debug)
    {
        if (value == nullptr){ //watch out for NULL
            cerr << "strset_insert: value is a NULL, aborted" << endl;
            return;
        }
        cerr << "strset_insert(" << id << ", \"" <<value 
            << "\")" << endl;
    }

    if (id == strset42)
    {
        if (debug)
            cerr << "strset_insert: can't insert values into strset42" 
                << endl;
        return;
    }

    auto iterator = stringSets().find(id);

    if (iterator != stringSets().end())
    { //there is a set with given ID
        if (iterator->second.count(value) == 0)
        { //value is not in the set
            iterator->second.insert(value);
            if(debug)
                cerr << "strset_insert: value \"" << value
                    << "\" added to set number " << id <<endl;
        }
        else
        { //value is allready in that set
            if(debug)
                cerr << "strset_insert: value \"" << value
                    << "\" already in set number " << id << endl;
        }
    }
    else
    { //no set with give ID
        if (debug)
        {
            cerr << "strset_insert: ";
            noSet(id);
        }
    }
}

extern "C" void strset_remove (unsigned long id, const char* value)
{
    //removes value from set with given ID
    init();

    if (debug)
    {
        if (value == nullptr){ //watch out for NULL
            cerr << "strset_remove: value is a NULL, aborted" << endl;
            return;
        }
        cerr << "strset_remove(" << id << ", \"" << value << "\")" << endl;
    }

    if (id == strset42)
    {
        if (debug)
            cerr << "strset_remove: can't remove values from strset42" 
                << endl;
        return;
    }

    if (stringSets().count(id) != 0)
    { //there is set with that ID
        if (stringSets()[id].erase(value) == 1)
        { //this set has that element
            if (debug)
                cerr << "strset_remove: value \"" << value
                    << "\" removed from set number " << id << endl;
        }
        else
        {//this set doesn't have that element
            if (debug)
                cerr << "strset_remove: set number " << id
                    << " does not contain value \""
                    << value << "\"" << endl;
        }
    }
    else
    {//no set with that value
        if (debug)
        {
            cerr << "strset_remove: ";
            noSet(id);
        }
    }
}

extern "C" int strset_test (unsigned long id, const char* value)
{
    init();

    if (debug)
    {
        if (value == nullptr)
        {
            cerr << "strset_test: value is a NULL, aborted" << endl;
            return 0;
        }
        cerr << "strset_test(" << id << ", \"" << value 
            << "\")" << endl;
    }

    if (stringSets().count(id) != 0)
    {
        if (stringSets()[id].count(value) != 0)
        {
            if (debug)
                cerr << "strset_test: set number " << id
                    << " contains the value \"" << value <<"\"" <<endl;
            return 1;
        }
        else
        {
            if (debug)
                cerr << "strset_test: set number " << id
                    <<" does not contain the value \"" << value 
                    << "\"" <<endl;
        }
    }
    else
        if (debug)
        {
            cerr << "strset_test: ";
            noSet(id);
        }
    return 0;
}

extern "C" void strset_clear (unsigned long id)
{
    init();

    if (debug)
        cerr << "strset_clear(" << id << ")" << endl;

    if (id == strset42)
    {
        if(debug)
            cerr << "strset_clear: can't clear strset42" << endl;
        return;
    }

    if (stringSets().count(id) != 0)
    {
        stringSets()[id].clear();
        if (debug)
            cerr << "strset_clear: cleared set number " << id << endl;
    }
    else
    {
        if(debug)
        {
            cerr << "strset_clear: ";
            noSet(id);
        }
    }
}

extern "C" int strset_comp (unsigned long id1, unsigned long id2)
{
    init();
    if (debug)
        cerr << "strset_comp(" << id1 <<", " << id2 << ")" << endl;

    const set<string>& set1 = getStrset(id1);
    const set<string>& set2 = getStrset(id2);

    if (debug)
        cerr << "strset_comp: the result of comparing set number " 
            << id1 << " with set number " << id2 << " is ";

    if (set1 == set2)
    {
        if (debug)
            cerr << "0" << endl;
        return 0;
    }
    else if (set1 < set2)
    {
        if (debug)
            cerr << "-1" << endl;
        return -1;
    }
    else
    {
        if (debug)
            cerr << "1" << endl;
        return 1;
    }
}
